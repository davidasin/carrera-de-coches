/**
 * Created by David on 31/10/2014.
 */
public class Coche extends Thread {

    private int velocidad;
    private int posicion;
    private int numeroCoche;
    private Circuito circuito;

    public Coche(Circuito circuito, int velocidad, int numeroCoche){
        super();
        this.velocidad = velocidad;
        this.numeroCoche = numeroCoche;
        this.circuito = circuito;
        this.posicion = 0;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public int getPosicion() {
        return posicion;
    }

    public int getNumeroCoche() {
        return numeroCoche;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public void run(){
        boolean fin = circuito.isHaGanado();
        while(!fin){
            fin = circuito.distanciaRecorrida(this);
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public String toString(){
        return String.valueOf(posicion);
    }
}
