/**
 * Created by David on 31/10/2014.
 */
public class Circuito {

    private int longitud;
    private boolean haGanado;

    public Circuito(int longitud) {
        this.longitud = longitud;
        this.haGanado = false;
    }

    public int getLongitud() {
        return longitud;
    }

    public synchronized boolean isHaGanado() {
        return haGanado;
    }

    public synchronized boolean distanciaRecorrida(Coche coche){
        if (!haGanado) {
            coche.setPosicion(coche.getPosicion() + coche.getVelocidad());
            System.out.println("El coche " + coche.getNumeroCoche() + " ha recorrido " + coche.getPosicion() + " metros");
            if (coche.getPosicion() >= longitud) {
                haGanado = true;
                System.out.println("El coche " + coche.getNumeroCoche() + " ha ganado la carrera");
            }

        }
        return haGanado;
    }
}
