import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Circuito circuito = new Circuito(10000);

        Coche[] listaCoches = new Coche[4];
        listaCoches[0] = new Coche(circuito, 20, 1);
        listaCoches[1] = new Coche(circuito, 25, 2);
        listaCoches[2] = new Coche(circuito, 20, 3);
        listaCoches[3] = new Coche(circuito, 25, 4);


        for (int i = 0; i < listaCoches.length; i++) {
            listaCoches[i].start();
        }

        for (int i = 0; i < listaCoches.length; i++) {
            try {
                listaCoches[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("FIN DE CARRERA");
        for (int i = 0; i < listaCoches.length; i++) {
            System.out.println("El coche "+listaCoches[i].getNumeroCoche()+" ha recorrido "+listaCoches[i].getPosicion());
        }
    }
}
